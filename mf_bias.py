from random import randint
import numpy as np
lr = 0.0002
n = 4
m = 5
k = 3
def mf(X,U,I,steps = 100000,beta=0):
    for _ in range(steps):
        for i in range(len(U)):
            for j in range(len(U[0])):
                if X[i][j] > 0:
                    eij = X[i][j] - np.dot(U[i,:],I[:,j])
                    for m in range(k):
                        U[i][m] = U[i][m] + lr * (2 * eij * I[m][j] - beta * U[i][m])
                        I[m][j] = I[m][j] + lr * (2 * eij * U[i][m] - beta * I[m][j])
        e = 0
        for i in range(len(U)):
            for j in range(len(U[0])):
                if X[i][j] >0:
                    e = e + pow(X[i][j] - np.dot(U[i,:],I[:,j]), 2)
        if e < 0.000001:
            break    
    return U,I
X = [
         [0,4,5,2],
         [1,1,3,5],
         [3,4,0,2],
         [2,0,3,4],
         [5,3,4,0],
        ]
print('X=')
print(X)

N = len(X)
M = len(X[0])
U = 5 * np.random.random_sample((N, k))
I = 5 * np.random.random_sample((k, M)) 


means_error = 99

print('U= ')
print(U)
    
print('I= ')
print(I)

X3 = np.dot(U,I)
print(X3)
nU,nI = mf(X,U,I)
print('X2=')
X2 = np.dot(nU,nI)
X2 = np.where(X2>5,5,X2)
X2 = np.where(X2<0,0,X2)
print(X2)