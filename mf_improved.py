import numpy as np
np.random.seed(3)
A = np.array([[1,4,5,0,3],
              [5,1,0,5,2],
              [4,1,2,5,0],
              [0,3,4,0,4]])
K = 3
W = np.random.rand(4, K)
H = np.random.rand(K, 4)
step =0
while(step < 1000):
    for u in range(0, 4):
        for i in range(0, 4):
            if A[u][i] >0 :
                r_bar =0
                for k in range(K):
                    r_bar += np.dot(W[u][k], H[k][i])
                eui = A[u][i] - r_bar
                for k in range(K):
                    W[u][k] += 2*0.01*(eui* H[k][i] - 0.001*W[u][k])
                    H[k][i] += 2*0.01*(eui* W[u][k] - 0.001*H[k][i])
    step +=1
MF = np.around(np.dot(W,H))
print('A: ', A)
print('MF: ', MF)