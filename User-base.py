import math
import numpy as np
X = [
         [5,3,0,1],
         [4,1,3,2],
         [3,4,0,5],
         [1,0,3,4],
         [1,1,5,0],
        ]
print('X= ')
rows = []
for i in range(len(X)):
    for j in range(len(X[0])):
        if X[i][j]==0:
            rows.append([i,j])
print(X)
for x in rows:
    cur = x[0]
    sum_sim = 0
    sum_sim_r = 0
    for i in range(len(X)):
        if i != cur:
            sum1 = 0
            sum2 = 0
            sum3 = 0
            for j in range (len(X[0])):
                if X[cur][j] != 0 and X[i][j] !=0:
                    sum1 += X[cur][j]*X[i][j] 
                    sum2 += X[cur][j]*X[cur][j]
                    sum3 += X[i][j]*X[i][j]
            cos = sum1/(math.sqrt(sum2*sum2)*math.sqrt(sum3*sum3))
            sum_sim += abs(cos)
            sum_sim_r += cos*X[i][j]
    X[x[0]][x[1]] = sum_sim_r/sum_sim
X = np.around(X)
print('Result = ')
print(X)
