import math
import numpy as np 
X = [
         [0,3,1,1],
         [4,1,3,2],
         [3,4,0,5],
         [1,0,3,4],
         [1,1,5,0],
        ]
print('X= ')
rows = []
for i in range(len(X)):
    for j in range(len(X[0])):
        if X[i][j]==0:
            rows.append([i,j])
print(X)
for x in rows:
    cur = x[1]
    sum_sim = 0
    sum_sim_r = 0
    for i in range(len(X[0])):
        if i != cur:
            sum1 = 0
            sum2 = 0
            sum3 = 0
            for j in range (len(X)):
                if X[j][cur] != 0 and X[j][i] !=0:
                    sum1 += X[j][cur]*X[j][i] 
                    sum2 += X[j][cur]*X[j][cur]
                    sum3 += X[j][i] *X[j][i] 
            cos = sum1/(math.sqrt(sum2*sum2)*math.sqrt(sum3*sum3))
            sum_sim += abs(cos)
            sum_sim_r += cos*X[j][i] 
    X[x[0]][x[1]] = sum_sim_r/sum_sim
X = np.around(X)
print("Resuilt: ")
print(X)