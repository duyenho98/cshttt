import math
import numpy as np
criteria = [
         [1,0.2,3,4],
         [0,1,0,0],
         [0,0,1,0],
         [0,0,0,1]
        ]
price = [
    [1,3,2],
    [0,1,0],
    [0,0,1],
]
distance = [
    [1,6,0.3],
    [0,1,0],
    [0,0,1],
]
labor = [
    [1,0.3,1],
    [0,1,0],
    [0,0,1],
]
wage = [
    [1,0.3,0.2],
    [0,1,0],
    [0,0,1],
]

labor = np.asarray(labor)
wage = np.asarray(wage)
distance = np.asarray(distance)
price = np.asarray(price)
criteria = np.asarray(criteria)

def fullFillMatrix(A):
    num_rows, num_cols = A.shape
    for i in range(0, num_rows):
        for j in range(0, num_cols):
            if A[i][j] == 0:
                if i < j - 1:
                    A[i][j] = A[i][j - 1] * A[j - 1][j]
                else:
                    A[i][j] = 1 / A[j][i]
    return A

def vectorize(R):
    R2 = np.dot(R,R)
    sum_vector = np.sum(R2,axis=1) 
    
    sumA = np.sum(sum_vector,axis= 0)
    sumA *= 1.0
    eff = sum_vector/sumA
    R3 = np.dot(R2,R2)
    sum_vector2 = np.sum(R3,axis=1)
    sum2 = np.sum(sum_vector2,axis= 0)
    eff2 = sum_vector2/sum2

    check = np.absolute(eff - eff2)
    condition  = check[check>0.5]

    while(len(condition)>0):
        R4 = np.dot(R3,R3)
        sum_vector2 = np.sum(R4,axis=1)
        sum2 = np.sum(sum_vector2,axis= 0)
        sum2 *= 1.0
        eff2 = sum_vector2/sum2
        check = np.absolute(eff - eff2)
        condition  = check[check>0.5]
        R3 = R4
    return eff2

criteria_f = fullFillMatrix(criteria)
labor_f = fullFillMatrix(labor)
wage_f = fullFillMatrix(wage)
price_f = fullFillMatrix(price)
distance_f = fullFillMatrix(distance)

labor_v = vectorize(labor_f)
wage_v = vectorize(wage_f)
distance_v = vectorize(distance_f)
price_v = vectorize(price_f)
criteria_v = vectorize(criteria_f)

matrix = np.stack((price_v,distance_v,labor_v,wage_v))
matrix_cri = np.reshape(criteria_v,(1,4))
result = np.dot(criteria_v,matrix)
print('result = ')
print(result)
