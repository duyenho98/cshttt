import math
import numpy as np
criteria = [
         [1,1/5,3,4],
         [5,1,9,7],
         [1/3,1/9,1,2],
         [1/4,1/7,1/2,1]
        ]
price = [
    [1,3,2],
    [1/3,1,1/5],
    [1/2,5,1],
]
distance = [
    [1,6,1/3],
    [1/6,1,1/9],
    [3,9,1],
]
labor = [
    [1,1/3,1],
    [3,1,7],
    [1,1/7,1],
]
wage = [
    [1,1/3,1/2],
    [3,1,4],
    [2,1/4,1],
]

labor = np.asarray(labor)
wage = np.asarray(wage)
distance = np.asarray(distance)
price = np.asarray(price)
# print(price)
criteria = np.asarray(criteria)

def vectorize(R):
    R2 = np.dot(R,R)
    sum_vector = np.sum(R2,axis=1) 
    
    sumA = np.sum(sum_vector,axis= 0)
    sumA *= 1.0
    eff = sum_vector/sumA
    R3 = np.dot(R2,R2)
    sum_vector2 = np.sum(R3,axis=1)
    sum2 = np.sum(sum_vector2,axis= 0)
    eff2 = sum_vector2/sum2

    check = np.absolute(eff - eff2)
    condition  = check[check>0.5]

    while(len(condition)>0):
        R4 = np.dot(R3,R3)
        sum_vector2 = np.sum(R4,axis=1)
        sum2 = np.sum(sum_vector2,axis= 0)
        sum2 *= 1.0
        eff2 = sum_vector2/sum2
        check = np.absolute(eff - eff2)
        condition  = check[check>0.5]
        R3 = R4
    return eff2

labor_v = vectorize(labor)
wage_v = vectorize(wage)
distance_v = vectorize(distance)
price_v = vectorize(price)
criteria_v = vectorize(criteria)
print(criteria_v)

matrix = np.stack((price_v,distance_v,labor_v,wage_v))
matrix_cri = np.reshape(criteria_v,(1,4))
result = np.dot(criteria_v,matrix)
print('result = ')

print(result)
